<?php

declare(strict_types=1);

namespace Weihu\DetectorConstant\DetectModule;

/**
 * 字段常量: 账号数目
 */
class AccountNumConstant
{

    /**
     * 七牛账号1: 
     */
    const QINIU_1 = 0;

    /**
     * 七牛账号2: 
     */
    const QINIU_2 = 1;


    #
}

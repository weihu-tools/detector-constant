<?php

declare(strict_types=1);

namespace Weihu\DetectorConstant\DetectModule;

/**
 * 字段常量: 探测器状态常量
 */
class DetectStatusConstant
{

    /**
     * 账号类型标识 : wechat-微信
     */
    const ACCOUNT_TYPE_WECHAT = 'wechat';

    /**
     * 账号类型标识 : qiniu-七牛
     */
    const ACCOUNT_TYPE_QINIU = 'qiniu';

    /**
     * 账号类型标识 : juming-聚名
     */
    const ACCOUNT_TYPE_JUMING = 'juming';

    /**
     * 探测结果状态 : 0-未知
     */
    const RESULT_STATUS_UNKNOW = 0;

    /**
     * 探测结果状态 : 1-在线
     */
    const RESULT_STATUS_ONLINE = 1;

    /**
     * 探测结果状态 : 2-离线
     */
    const RESULT_STATUS_OFFLINE = 2;

    /**
     * 探测结果状态 : 3-连接超时
     */
    const RESULT_STATUS_TIMEOUT = 3;

    /**
     * 探测结果状态 : 4-程序错误
     */
    const RESULT_STATUS_CODE_ERROR = 4;

    /**
     * 探测结果状态 : 5-接口异常
     */
    const RESULT_STATUS_API_EXCEPTION = 5;

    /**
     * 探测结果状态 : 6-网络掉线
     */
    const RESULT_STATUS_OFF_NETWORK = 6;

    /**
     * 探测结果状态 : 7-解析超时
     */
    const RESULT_STATUS_RESOLVE_TIMEOUT = 7;

    /**
     * 探测结果状态 : 8-操作超时
     */
    const RESULT_STATUS_OPERATION_TIMEOUT = 8;

    /**
     * 探测结果状态 : 
     *  0-状态未知 
     *  1-在线 
     *  2-离线 
     *  3-连接超时 
     *  4-程序错误 
     *  5-接口异常 
     *  6-网络掉线 
     *  7-解析超时 
     *  8-操作超时
     */
    const RESULT_STATUS_LISTS = [
        self::RESULT_STATUS_UNKNOW => '未知',
        self::RESULT_STATUS_ONLINE => '在线',
        self::RESULT_STATUS_OFFLINE => '离线',
        self::RESULT_STATUS_TIMEOUT => '连接超时',
        self::RESULT_STATUS_CODE_ERROR => '程序错误',
        self::RESULT_STATUS_API_EXCEPTION => '接口异常',
        self::RESULT_STATUS_OFF_NETWORK => '网络掉线',
        self::RESULT_STATUS_RESOLVE_TIMEOUT => '解析超时',
        self::RESULT_STATUS_OPERATION_TIMEOUT => '操作超时',
    ];

    /**
     * 登录状态: 0-未知
     */
    const LOGIN_STATUS_ONKNOW = 0;

    /**
     * 登录状态: 1-在线
     */
    const LOGIN_STATUS_ONLINE = 1;

    /**
     * 登录状态: 2-离线
     */
    const LOGIN_STATUS_OFFLINE = 2;

    /**
     * 登录状态 : 0-未知 , 1-在线 , 2-离线
     */
    const LOGIN_STATUS_LISTS = [
        self::LOGIN_STATUS_ONKNOW => '未知',
        self::LOGIN_STATUS_ONLINE => '在线',
        self::LOGIN_STATUS_OFFLINE => '离线',
    ];

    /**
     * 微信域名状态 : 0-未知
     */
    const WECHAT_MI_STATUS_ONKNOW = 0;

    /**
     * 微信域名状态 : 1-正常
     */
    const WECHAT_MI_STATUS_NORMAL = 1;

    /**
     * 微信域名状态 : 2-转码
     */
    const WECHAT_MI_STATUS_TRANSCODING = 2;

    /**
     * 微信域名状态 : 3-被封
     */
    const WECHAT_MI_STATUS_BANNED = 3;

    /**
     * 微信域名状态 : 0-未知 , 1-正常 , 2-转码 , 3-被封
     */
    const WECHAT_MI_STATUS_LISTS = [
        self::WECHAT_MI_STATUS_ONKNOW => '未知',
        self::WECHAT_MI_STATUS_NORMAL => '正常',
        self::WECHAT_MI_STATUS_TRANSCODING => '转码',
        self::WECHAT_MI_STATUS_BANNED => '被封',
    ];


    #
}

<?php

declare(strict_types=1);

namespace Weihu\DetectorConstant;


/**
 * 字段常量: 缓存失效时间
 */
class CacheTTLConstant
{

    /**
     * 失效时间(秒数): 1分钟(60秒)
     */
    const TTL_MINUTE = 60;

    /**
     * 失效时间(秒数): 10分钟(600秒)
     */
    const TTL_TEN_MINUTE = 600;

    /**
     * 失效时间(秒数): 1天(86400秒)
     */
    const TTL_DAY = 86400;

    /**
     * 失效时间(秒数): 7天(604800秒)
     */
    const TTL_WEEK = 604800;

    /**
     * 失效时间(秒数): 30天(2592000秒)
     */
    const TTL_MONTH = 2592000;


    #
}

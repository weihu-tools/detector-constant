<?php

declare(strict_types=1);

namespace Weihu\DetectorConstant;

/**
 * 字段常量: 缓存key
 */
class CacheKeyConstant
{
    /**
     * 字母前缀(用于md5字符串后防止数字在前)
     */
    const PRE = 'pre';

    /**
     * 标识: redis锁前缀
     */
    const REDIS_LOCK_PRE = 'rlock';

    /**
     * 标识: 微信
     */
    const MARK_WECHAT = 'wechat';

    /**
     * 标识: 聚名
     */
    const MARK_JUMING = 'juming';

    /**
     * 标识: 七牛
     */
    const MARK_QINIU = 'qiniu';

    /**
     * 本次操作时间:wechat
     */
    const CK_DETECT_WECHAT_THIS_TIME = "wechat-this-time";

    /**
     * 账号登录状态:wechat
     */
    const CK_DETECT_WECHAT_LOGIN_STATUS = "wechat-login-status";

    /**
     * 本次操作时间:qiniu
     */
    const CK_DETECT_QINIU_THIS_TIME = "qiniu-this-time";

    /**
     * 账号登录状态:qiniu
     */
    const CK_DETECT_QINIU_LOGIN_STATUS = "qiniu-login-status";

    /**
     * 本次操作时间:juming
     */
    const CK_DETECT_JUMING_THIS_TIME = "juming-this-time";

    /**
     * 账号登录状态:juming
     */
    const CK_DETECT_JUMING_LOGIN_STATUS = "juming-login-status";

    /**
     * 聚名域名列表ck : redis的set类型
     */
    const CK_JUMING_LIST_TYPE_SET = 'juming-list-type-set';

    /**
     * 缓存域名详情的前缀键
     */
    const CK_JUMING_DETAIL_PRE = 'juming-detail-pre';

    #
}

<?php

declare(strict_types=1);

namespace Weihu\DetectorConstant;


/**
 * 字段常量: 配置项的键
 */
class ConfigKeyConstant
{

    /**
     * 账号配置文件名: 微信
     */
    const FILE_WECHAT = 'account-wechat';

    /**
     * 账号配置文件名: 七牛
     */
    const FILE_QINIU = 'account-qiniu';

    /**
     * 账号配置文件名: 聚名
     */
    const FILE_JUMING = 'account-juming';

    /**
     * 'uri'的后缀
     */
    const KEY_URI_SUBFIX = 'uri';

    /**
     * 'cookie'的后缀
     */
    const KEY_COOKIE_SUBFIX = 'cookie';

    /**
     * '更新键'的后缀
     */
    const KEY_UP_KEY_SUBFIX = 'up_key';

    /**
     * '账号名'的后缀
     */
    const KEY_NAME_KEY_SUBFIX = 'name';



    #
}

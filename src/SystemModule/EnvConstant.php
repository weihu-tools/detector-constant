<?php

declare(strict_types=1);

namespace Weihu\DetectorConstant\SystemModule;

/**
 * 字段常量: 应用环境
 */
class EnvConstant
{

    /**
     * 应用环境: local-本地开发
     */
    const ENV_LOCAl = 'local';

    /**
     * 应用环境: dev-线上开发
     */
    const ENV_DEV = 'dev';

    /**
     * 应用环境: prod-生产环境
     */
    const ENV_PROD = 'PROD';

    #
}

<?php

declare(strict_types=1);

namespace Weihu\DetectorConstant\SystemModule;


/**
 * 字段常量: env配置项的键
 */
class EnvKeyConstant
{

    /**
     * 应用环境 APP_ENV
     */
    const KEY_APP_ENV = 'APP_ENV';

    /**
     * TENCENT_COS_SECRETID
     */
    const KEY_TENCENT_COS_SECRETID = 'TENCENT_COS_SECRETID';

    /**
     * TENCENT_COS_SECRETKEY
     */
    const KEY_TENCENT_COS_SECRETKEY = 'TENCENT_COS_SECRETKEY';

    /**
     * TENCENT_COS_REGION
     */
    const KEY_TENCENT_COS_REGION = 'TENCENT_COS_REGION';

    /**
     * TENCENT_COS_BUCKET
     */
    const KEY_TENCENT_COS_BUCKET = 'TENCENT_COS_BUCKET';

    #
}
